using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseInteraction : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    private Rigidbody physique;
    private Renderer materiel;
    private Color color;
    public void OnPointerClick(PointerEventData eventData)
    {
        physique.AddForce(Camera.main.transform.forward * 20000); // on ajoute une force liée au click de la souris
        
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        materiel.material.color = color; // on ne survole plus l'objet, on remet la couleur qu'il avait avant
    }

    void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData)
    {
        color = this.materiel.material.color; // on stock la couleur qu'il y avait avant de la modifier
        materiel.material.color = Color.red; // on change la couleur à rouge
        
    }

    // Start is called before the first frame update
    void Start()
    {
        physique = this.GetComponent<Rigidbody>(); // on initialise l'attribut physique
        materiel = this.GetComponent<Renderer>(); // on initialise l'attribut materiel





    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
